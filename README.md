Application demonstrates the spring attacks. 

* This application is tested on Java Enterprise Edition 7, Maven2, JBOSS and MySQL 5.6. 

*  Import this project as existing maven project into workspace. 
*  Update the file database.properties which is in resource package as per your Database details. 

* Src/main/java folder contains back end files written in Java. 
* Src/main/webapp/views contains all the JSPs. Src/main/webapp/WEB-INF/ contains web.xml and configuration files. 
* Required SQL Scripts are in separate file named sqlscripts.
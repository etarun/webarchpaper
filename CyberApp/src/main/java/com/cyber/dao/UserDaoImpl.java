package com.cyber.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.SessionFactory;

import com.cyber.entity.User;
import com.cyber.entity.UserRole;

public class UserDaoImpl implements UserDao {

	private SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	@SuppressWarnings("unchecked")
	public User findByUserName(String username) {

		List<User> users = new ArrayList<User>();

		users = sessionFactory.getCurrentSession().createQuery("from User where username=?")
				.setParameter(0, username).list();

		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}

	}

	@SuppressWarnings("unchecked")
	public Set<UserRole> getUserRoles(User user) {

		List<UserRole> roles = new ArrayList<UserRole>();

		roles = sessionFactory.getCurrentSession().createQuery("from UserRole where user=?")
				.setParameter(0, user).list();

		if (roles.size() > 0) {
			return new HashSet<UserRole>(roles);
		} else {
			return null;
		}

	}
}
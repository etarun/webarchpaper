package com.cyber.dao;

import java.util.Set;

import com.cyber.entity.User;
import com.cyber.entity.UserRole;

public interface UserDao {

	User findByUserName(String username);

	Set<UserRole> getUserRoles(User user);

}